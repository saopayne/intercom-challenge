## Intercom Challenge

#### Proudest Achievement

In my previous company, we started with 3 customers and processing approximately 7k requests of incoming data point. The system is a data ingestion system for company's messages which is responsible for data transformation through a machine learning pipeline to extract useful data points, classification, data tagging which are then presented to the customers on a dashboard as actionable insights.

The data grew 10x - 15x to the range of 100k requests per day, we started hitting some bottlenecks as our system kept going offline constantly.

I led the efforts around making this scalable and get the uptime close to 99.9%. We ran a monolith in Groovy & Grails and while that wasn't an issue in itself, the workload of our customers created some bottlenecks for us.

I introduced decoupling the systems into micro-services along the domain, breaking it into additional separate services initially (payments, integrations, dashboard and the main system) which were built with Java.

I then introduced some queuing system, RabbitMQ, to improve the reliability of the systems. Further, in a bid to improve the latency of the system, I integrated the first caching system(Redis) which had some eviction policy based on LFU which served our needs the best.

Further, to test the system, we had to subject it to some stress-testing. I built a load-testing tool in Python which further identified some other hotspots.


#### 1. Technical problem

We have some customer records in a text file (customers.txt) -- one customer per line, JSON lines formatted. We want to invite any customer within 100km of our Dublin office for some food and drinks on us. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).

##### Submission by Ademola Oyewale (saopayne@gmail.com)

###### Submission Date: 08-07-2020

#### Tools Used in the project

 - Java 1.8
 - Maven
 - Docker
 
#### Project Architecture
 
 The architecture of the application uses MVC because of separated presentation. 

 The project thus has these components below based on separation of concerns:
 
     * service: responsible for the business logic.
     * domains: functional objects which represent the entities in the application.
 
 - Docker to containerize the application which provides an out-of-the box run experience
 
#### Decisions Made

- The file containing the information about the customers is hardcoded into the application.
 
- Git for versioning.
     
#### Running up the application

###### A. Locally

Requirements: Java is setup on the local machine [guide](https://www.tutorialspoint.com/java/java_environment_setup.htm)    

    $ mvn clean
    $ mvn install
    $ mvn exec:java -Dexec.mainClass="intercom.services.CustomerInviteService"
    
This will print the eligible customers into `output.txt` and also print the list to the terminal.

#### Testing

 `$ mvn test`
 
 Please run this command from the root of the project.
 
#### Output of program

    
    {"user_id":4,"name":"Ian Kehoe"}
    {"user_id":5,"name":"Nora Dempsey"}
    {"user_id":6,"name":"Theresa Enright"}
    {"user_id":8,"name":"Eoin Ahearn"}
    {"user_id":11,"name":"Richard Finnegan"}
    {"user_id":12,"name":"Christina McArdle"}
    {"user_id":13,"name":"Olive Ahearn"}
    {"user_id":15,"name":"Michael Ahearn"}
    {"user_id":17,"name":"Patricia Cahill"}
    {"user_id":23,"name":"Eoin Gallagher"}
    {"user_id":24,"name":"Rose Enright"}
    {"user_id":26,"name":"Stephen McArdle"}
    {"user_id":29,"name":"Oliver Ahearn"}
    {"user_id":30,"name":"Nick Enright"}
    {"user_id":31,"name":"Alan Behan"}
    {"user_id":39,"name":"Lisa Ahearn"}
   

#### Further Improvements

- Allow the input file to be passed in dynamically. 

- The output can also be exposed over a frontend
