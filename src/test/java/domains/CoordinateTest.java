package domains;

import intercom.domains.Coordinate;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CoordinateTest {

    @Test
    public void testEquals() {
        assertEquals(new Coordinate(0.0, 0.3), new Coordinate(0.0, 0.3));
        assertEquals(new Coordinate(0.000, 0.3000), new Coordinate(0.0, 0.3));
        assertEquals(new Coordinate(1.0000, 0.3), new Coordinate(01.0, 0.3));
        assertEquals(new Coordinate(1.0000, 00000.3), new Coordinate(01.0, 0.3));
        assertEquals(new Coordinate(1.000000000000000, 00000.3), new Coordinate(01.0, 0.3));
    }

    @Test
    public void testNotEquals() {
        assertNotEquals(new Coordinate(56.44,-9.444), new Coordinate(10.023434, 0.344));
        assertNotEquals(new Coordinate(-9.44,-9.444), new Coordinate(10.023434, 0.34));
        assertNotEquals(new Coordinate(56.44,56.44), new Coordinate(10.023434, 0.344));
        assertNotEquals(new Coordinate(56.44,-9.441), new Coordinate(56.44, -6.441));
        assertNotEquals(new Coordinate(-1.0000,1.0000), new Coordinate(1.0000, -1.0000));
    }
}
