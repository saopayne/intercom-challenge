package domains;

import intercom.domains.Coordinate;
import intercom.domains.Customer;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;

public class CustomerTest {

    @Test
    public void testEquals() {
        Customer customerA = new Customer("Ademola", 1, new Coordinate(0.0, 0.3));
        Customer customerB = new Customer("Ademola", 1, new Coordinate(0.0, 0.3));
        assert(customerA.equals(customerB));

        Customer customerC = new Customer("Ademola", 1, new Coordinate(0.000, 0.3));
        Customer customerD = new Customer("Ademola", 1, new Coordinate(0.0, 0.3));
        assert(customerC.equals(customerD));

        Customer customerE = new Customer("Ademola", 1, new Coordinate(0.0, 0.300));
        Customer customerF = new Customer("Ademola", 1, new Coordinate(0.0, 0.3));
        assert(customerE.equals(customerF));
    }

    @Test
    public void testNotEquals() {
        assertNotEquals(new Customer("Ademola", 1, new Coordinate(0.0, 0.3)), new Customer("other name", 2, new Coordinate(0.0, 0.3)));
        assertNotEquals(new Customer("Rodr", 1, new Coordinate(0.0, 0.3)), new Customer("Rodr", 2, new Coordinate(0.0, 0.3)));
        assertNotEquals(new Customer("rodr", 1, new Coordinate(0.0, 0.3)), new Customer("Rodr", 2, new Coordinate(0.0, 0.3)));
        assertNotEquals(new Customer("name", 1, new Coordinate(0.0, 0.3)), new Customer("name", 1, new Coordinate(0.1, 0.3)));
        assertNotEquals(new Customer("name", 1, new Coordinate(0.0, 0.3)), new Customer("name", 1, new Coordinate(0.0, 0.4)));
        assertNotEquals(new Customer("name", 1, new Coordinate(0.0, 0.3)), new Customer("other name", 2, new Coordinate(0.1, 0.4)));
    }
}
