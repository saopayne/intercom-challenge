package services;

import intercom.domains.Coordinate;
import intercom.services.CustomerInviteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;


@RunWith(Parameterized.class)
public class CustomerInviteServiceTest {

    @Parameterized.Parameters(name = "Test {index}: distanceFromOfficeInKm({0})={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new Coordinate(52.986375, -6.043701), true },
                { new Coordinate(51.92893,-10.27699), false },
                { new Coordinate(51.8856167, -10.4240951), false },
                { new Coordinate(52.3191841, -8.5072391), false },
                { new Coordinate(53.807778, -7.714444), false },
                { new Coordinate(54.374208, -8.371639), false },
                { new Coordinate(52.833502, -8.522366), false },
                { new Coordinate(53.1489345, -6.8422408), true },
                { new Coordinate(1000, -1000), false },
                { new Coordinate(0, 0), false },
                { new Coordinate(-10, -10), false },
        });
    }

    private Coordinate coordinate;

    private boolean isWithinDistance;

    private CustomerInviteService service = new CustomerInviteService();

    public CustomerInviteServiceTest(Coordinate c, boolean expected) {
        coordinate = c;
        isWithinDistance = expected;
    }

    @Test
    public void testDistanceFromOfficeInKm() {
        assertEquals(isWithinDistance, service.distanceFromOfficeInKm(coordinate) < 100);
    }

}