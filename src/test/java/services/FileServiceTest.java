package services;

import intercom.domains.Coordinate;
import intercom.domains.Customer;
import intercom.services.FileService;
import org.json.JSONException;
import org.junit.Test;

public class FileServiceTest {

    @Test
    public void testJsonStringToCustomer() throws JSONException {
        FileService fileService = new FileService();

        String jsonString = "{\"latitude\": 52.986375, \"user_id\": 12, \"name\": \"Oyewale Ademola\", \"longitude\": -6.043701}";
        Customer customerFromJson = fileService.jsonStringToCustomer(jsonString);
        Customer customerFromObject = new Customer("Oyewale Ademola", 12, new Coordinate(52.986375, -6.043701));

        assert(customerFromJson.equals(customerFromObject));
    }
}
