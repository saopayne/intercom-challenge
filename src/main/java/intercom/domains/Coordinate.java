package intercom.domains;

public class Coordinate {

    private double latitude;
    private double longitude;

    public Coordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object obj){
        if (obj instanceof Coordinate) {
            Coordinate coord = (Coordinate) obj;
            return Double.compare(this.getLatitude(), coord.getLatitude()) == 0 &&
                    Double.compare(this.getLongitude(), coord.getLongitude()) == 0;
        }

        return false;
    }

    @Override
    public String toString() {
        return "{" +
                    "latitude: " + getLatitude() + "," +
                    "longitude: " + getLongitude() +
                "}";
    }

}
