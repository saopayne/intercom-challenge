package intercom.domains;

public class Customer {

    private String name;
    private int userId;

    private Coordinate coordinate;

    public Customer(String name, int userId, Coordinate coordinate) {
        this.name = name;
        this.userId = userId;
        this.coordinate = coordinate;
    }

    public String getName() {
        return name;
    }

    public int getUserId() {
        return userId;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    @Override
    public boolean equals(Object obj){
        if (obj instanceof Customer) {
            Customer customer = (Customer) obj;
            return this.getName().equals(customer.getName()) &&
                    this.getUserId() == customer.getUserId() &&
                    this.getCoordinate().getLongitude() == customer.getCoordinate().getLongitude() &&
                    this.getCoordinate().getLatitude() == customer.getCoordinate().getLatitude();
        }

        return false;
    }

    @Override
    public String toString() {
        return "{" +
                    "Name: " + getName() + "," +
                    "User ID: " + getUserId() + "," +
                    "Coordinate: " + getCoordinate() +
                "}";
    }

}
