package intercom.services;

import intercom.domains.Coordinate;
import intercom.domains.Customer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CustomerInviteService {

    private static final double DUBLIN_OFFICE_LATITUDE = 53.339428;
    private static final double DUBLIN_OFFICE_LONGITUDE = -6.257664;
    private static final double EARTH_RADIUS = 6372.8;

    /**
     * Calculates the distance in km between two coordinates (customer and the office)
     *
     * @param customerCoordinate: the coordinates of the customer (lat, lon)
     * @return The distance in km
     */
    public double distanceFromOfficeInKm(Coordinate customerCoordinate) {
        double customerLatitude = customerCoordinate.getLatitude();
        double customerLongitude = customerCoordinate.getLongitude();

        double latitudeDistance = Math.toRadians(customerLatitude - DUBLIN_OFFICE_LATITUDE);
        double longitudeDistance = Math.toRadians(customerLongitude - DUBLIN_OFFICE_LONGITUDE);

        double underSqRoot = Math.pow(Math.sin(latitudeDistance / 2), 2) + Math.pow(Math.sin(longitudeDistance / 2), 2)
                * Math.cos(Math.toRadians(customerLatitude)) * Math.cos(Math.toRadians(DUBLIN_OFFICE_LATITUDE));

        double c = 2 * Math.asin(Math.sqrt(underSqRoot));

        return EARTH_RADIUS * c;
    }

    /**
     * Filter out customers who are eligible for the invite to the Dublin office :)
     *
     * @param customersFile: the file name (+ relative path) containing customers
     * @return Map of user ID and Customer
     * @throws IOException
     */
    private Map<Integer, Customer> filterInvitedCustomers(String customersFile) throws IOException {
        FileService fileService = new FileService();

        List<Customer> customers = fileService.getCustomerList(customersFile);

        Map<Integer, Customer> eligibleCustomers = new TreeMap<>();

        for (Customer customer : customers) {
            if (distanceFromOfficeInKm(customer.getCoordinate()) < 100) {
                eligibleCustomers.put(customer.getUserId(), customer);
            }
        }

        return eligibleCustomers;
    }

    public static void main(String[] args) throws IOException {
        CustomerInviteService customerInviteService = new CustomerInviteService();
        Map<Integer, Customer> eligibleCustomers = customerInviteService.filterInvitedCustomers("customers.txt");

        FileWriter writer = new FileWriter("output.txt");

        for (Map.Entry<Integer, Customer> entry : eligibleCustomers.entrySet()){
            Customer customer = entry.getValue();

            JSONObject obj = new JSONObject();
            try {
                obj.put("name", customer.getName());
                obj.put("user_id", customer.getUserId());
                writer.write(obj.toString() + "\n");
                System.out.println(obj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        writer.flush();
        writer.close();

    }

}
