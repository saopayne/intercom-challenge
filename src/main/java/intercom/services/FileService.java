package intercom.services;

import intercom.domains.Coordinate;
import intercom.domains.Customer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileService {

    /**
     * Converts the json representation of the customer to a customer object
     *
     * @param customerString: the JSON string of the customer
     * @return Customer: the object formed from the string
     */
    public Customer jsonStringToCustomer(String customerString) throws JSONException {
        JSONObject customerJSON = new JSONObject(customerString);

        double latitude = customerJSON.getDouble("latitude");
        double longitude = customerJSON.getDouble("longitude");
        String name = customerJSON.getString("name");
        int userID = customerJSON.getInt("user_id");

        Coordinate customerCoordinate = new Coordinate(latitude, longitude);

        return new Customer(name, userID, customerCoordinate);
    }

    /**
     * collects the list of customers from the JSON representation
     *
     * @return List: the list of customers
     * @throws IOException
     */
     List<Customer> getCustomerList(String customersFile) throws IOException {
         BufferedReader bufferedReader;

         try {
             bufferedReader = loadFile(customersFile);
         } catch (IllegalArgumentException iae) {
             System.out.println("Error while reading the file: " + iae.getMessage());

             return Collections.emptyList();
         }

        String jsonCustomerString;

        List<Customer> allCustomers = new ArrayList<>();

        while ((jsonCustomerString = bufferedReader.readLine()) != null) {
            try {
                Customer customer = jsonStringToCustomer(jsonCustomerString);
                allCustomers.add(customer);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return allCustomers;
    }

    private BufferedReader loadFile(String path) {
        try {
            return new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Error reading file: " + e.getLocalizedMessage());
        }
    }

}
